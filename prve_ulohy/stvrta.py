from typing import List
from prva import getInputListWithNumbers

def findMaxIndex(array: List):
    index_of_max = 0
    for i in range(1, len(array)):
        if array[i] > array[index_of_max]:
            index_of_max = i

    return index_of_max

if __name__ == '__main__':
    n = int(input('Zadaj kolko prvkov ma mat pole: '))
    l = getInputListWithNumbers(length=n)
    
    print()
    print(l)
    print(f'Najvacsie cislo sa nachadza na pozicii: {findMaxIndex(l)+1}')