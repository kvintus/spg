from prva import getInputListWithNumbers

def bubbleNonZeroElementsToLeft(l):
    """ Inplace bubbling """
    for i in range(n):
        if l[i] == 0:
            next_el = i + 1
            # Bubble to till end
            while next_el < n:
                # Swap
                if l[next_el] != 0:   
                    l[i] = l[next_el]
                    l[next_el] = 0
                    break
                next_el += 1
    
    return l

if __name__ == "__main__":
    n = int(input('Zadaj pocet prvkov: '))
    povodne = getInputListWithNumbers(length=n)
    print(povodne)
    bubbleNonZeroElementsToLeft(povodne)

    try:
        index_of_first_zero = povodne.index(0)

        print(povodne[:index_of_first_zero])
        print(index_of_first_zero)
    except:
        print(povodne)
        print(len(povodne))

