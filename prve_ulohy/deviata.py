from typing import List, Dict
from prva import getInputListWithNumbers

def getLongestSimetric(pole: List[int]) -> Dict[str, int]:
    rad = {
        'index': None,
        'length': 0
    }

    for i in range(len(pole)):
        move = 0
        try:
            # ta prva podmienka tam je na prevenciu proti pythonovskemu slicovaniu

            while i-move >= 0 and pole[i-move] == pole[i+move]:
                move += 1
        except:
            pass
        if move > 1:
            current_length = 2*(move-1)+1
            if current_length > rad['length']:
                rad['index'] = i-move+1
                rad['length'] = current_length
    
    return rad


if __name__ == '__main__':
    pole = getInputListWithNumbers(length=int(input('Zadaj dlzku pola: ')))
    
    # Test data
    # pole = [0,1,2,1,7,9,4,5,4,9,7, 1, 2, 1]
    # pole = [1,2,1,0,5]
    
    res = getLongestSimetric(pole)
    print()
    print(pole)
    print('Pociatok: {poc}\nDlzka: {dlz}'.format(poc=res['index']+1, dlz=res['length']))