from typing import List

def rozlozNaPrvo(x: int) -> List[int]:
    res = []
    for i in range(2, x+1):
        while x%i == 0:
            x /= i
            res.append(i)

    return res


print(rozlozNaPrvo(int(input('Zadaj cislo: '))))