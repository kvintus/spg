from typing import List

def isEven(number: int) -> bool:
    return not number % 2


def getInputListWithNumbers(length: int=3) -> List:
    cisla = []
    for i in range(length):
        cisla.append(int(input("Zadaj {}. cislo: ".format(i+1))))

    return cisla


if __name__ == "__main__":
    cisla = getInputListWithNumbers(length=3)
    cisla = list(filter(isEven, cisla))
    print('\nParnych cisel v poli je: {}\nA to su: {}'.format(len(cisla), cisla))