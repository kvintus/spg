import prva as p


if __name__ == "__main__":
    cisla = p.getInputListWithNumbers()
    cisla.sort(reverse=True)

    print('Najvacsie cislo ({}) {} neparne'.format(cisla[0], 'je' if not p.isEven(cisla[0]) else 'nie je'))
