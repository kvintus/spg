def convertUniversal(current: int, required_for_next: int):
    basket = 0
    nextt = 0
    while True:
        basket += 1
        if current < required_for_next:
            break
        if basket == required_for_next:
            basket = 0
            current -= required_for_next
            nextt += 1
    
    return current, nextt

if __name__ == '__main__':
    hours, days = convertUniversal(int(input('Zadaj pocet hodin: ')), 24)
    days, weeks = convertUniversal(days, 7)

    print('Hodin: {}\nDni: {}\nTyzdne: {}'.format(hours, days, weeks))
    