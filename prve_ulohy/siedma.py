from prva import getInputListWithNumbers
from math import sqrt

def uhlopriecka(a: int, b: int, c: int):
    return round(sqrt((a**2)+(b**2)+(c**2)), 2)

if __name__ == "__main__":
    values = getInputListWithNumbers(3)
    print(f'Telesova uhlopriecka je: {uhlopriecka(values[0], values[1], values[2])}')