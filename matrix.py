from typing import List, Tuple
from fractions import Fraction

class Matrix:
    def __init__(self, matrix=None, pila=False):
        self.pila = pila
        if not matrix:
            self.size = int(input('Sustava o: '))
            self.matrix = self.fillFromUserInput(self.n)
        else:
            self.size = len(matrix)
            self.matrix = matrix
        
        # Use fractions for more accurate results
        if not self.pila:
            for i in range(self.size):
                for j in range(self.size + 1):
                    self.matrix[i][j] = Fraction(self.matrix[i][j])

    def __str__(self):
        v = ''
        for row_index in range(self.size):
            v += f'{self.matrix[row_index]}\n'
        return v

    def fillFromUserInput(self, n: int) -> List[List[int]]:
        """
        Fills the matrix from user input
        """
        res = []
        for i in range(n):
            row = list(map(lambda x: int(x), input(f'Riadok: {i}').split(' ')))
            res.append(row)

        return res

    def swapRows(self, first_index: int, second_index: int):
        """
        Swaps the rows in the matrix
        """
        self.matrix[second_index], self.matrix[first_index] = self.matrix[first_index], self.matrix[second_index]
    
    def canBeSwapped(self, i: int) -> bool:
        """
        Gets the 'i' as an input which is both x and y coordinate of the variable that should be '1'
        Checks if there are any rows underneath the one indicated with 'i'. If there are, the functions swaps 
        them and returns True
        """
        for j in range(i, self.size):
            # Check if the any of them is 1 on the i-th place
            if self.matrix[j][i] == 1:
                self.swapRows(i, j)
                return True
        return False
    


    def getIntoSolbeableState(self):
        """
        Gets the matrix into state from which we can solve the matrix
        """
        # Go through all the rows
        for i in range(self.size):
            # Swap if can be swapped, perform division otherwise
            if not self.canBeSwapped(i=i):                
                # Check if the current element is 0
                if self.matrix[i][i] == 0:
                    if self.size-1 == i:
                        self.raiseMatrixIsSingular()
                    # if it is swap current row with the next row
                    self.swapRows(i, i+1)
                
                to_multiply_w = 1/self.matrix[i][i]
                for elem_index in range(self.size + 1):
                    self.matrix[i][elem_index] *= to_multiply_w
            
            # Null all the variables at the i-th column in the rows whose index is higher than i
            for rem_row_index in range(i+1, self.size):
                # multiply i-th row by the reversed value of var that is in the rem_row
                # and i-th column
                to_multiply_with = (-self.matrix[rem_row_index][i])
                for col_index in range(self.size + 1):
                    self.matrix[rem_row_index][col_index] += to_multiply_with * self.matrix[i][col_index]

    def raiseMatrixIsSingular(self):
        raise Exception('Matica je singularna')

    def solve(self) -> Tuple:
        """
        Solves the matrix
        """
        self.getIntoSolbeableState()
        # Check if is sing.
        if not self.matrix[self.size-1][self.size-1]:
            self.raiseMatrixIsSingular()

        results = [self.matrix[self.size-1][self.size]]
        for row_index in range(self.size-2, -1, -1):
            res_of_this_row = self.matrix[row_index][self.size]
            for col_index in range(row_index+1, self.size):
                res_of_this_row += -(self.matrix[row_index][col_index] * results[self.size - (col_index + 1)])
            results.append(res_of_this_row)
        

        results.reverse()
        # if we are not in pila mode we need to convert the fractions first
        if not self.pila:
            return tuple(map(lambda x: float(x), results))
        else:    
            return tuple(results)





if __name__ == "__main__":
    # n = int(input('Sustava o: '))
    # matrix = get2DArray(n, n+1)
    test_m = [
        [2, 3, -4, -3],
        [1, -4, 2, 8],
        [3, 1, -5, 0]
        # [0,0,0,0]
    ]

    matrix = Matrix(matrix=test_m, pila=False)
    print(matrix.solve())